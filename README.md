# MJV Rookies - Estamos contratando!

A MJV está em busca de devenvolvedores da próxima geração.
Nesta busca, estamos procurando candidatos com perfies seguintes:
- Front-end - Junior
- Back-end - Junior
- Fullstack

Para se candidatar em umas destas vagas é necessário criar uma aplicação que seja capaz de demostrar os seus conhecimentos.


## Front-end - Junior
Construir uma aplicação SPA utilizando Angular(ver. acima de 2), Sass e TypeScript que atenda o seguinte:

- O usuário terá 1 tela que lista os títulos dos "Post".
- Ao clicar no título do "Post", deve exibir os detalhes com as informações complementares como o nome e e-mail do Autor(que está cinvulado com userId).
- As informações devem ser consumida pelo [JSONPlaceholder](https://jsonplaceholder.typicode.com/)


## Back-end - Junior
Construir uma aplicação NodeJs utilizando JavaScript ou TypeScript com 3 endpoints seguindo o padrão do [JSONPlaceholder](https://jsonplaceholder.typicode.com/).
A aplicação deve utilizar banco relacional ou NoSQL.

- Construir um endpoint get "/posts" que exibe todos os "Post".
- Construir um endpoint get "/posts/{id}" que exibe o detalhe do "Post" selecionado.
- Construir um endpoint get "/user/{id}" que exibe o detalhe do "User".
- As informações dos "Posts" e "User" devem ser inseridas na inicialização da aplicação.


## Fullstack

- Criar a aplicação do Front-end e Back-end com a integração entre eles. 
- Criar uma nova tela para Criação e edição de "Post".
- Criar um endpoint post "/posts" para insersão  de "Post.
- Criar um endpoint put "/posts/{id}"para edição  de "Post.


## Requisitos técnicos para todas aplicações
No "readMe.md" de incluir as informações abaixo.

- Instruções para build, deploy e execução.
- Comentário com justificativa das decisões técnicas.


## Como enviar o código
Criar um projeto no seu Git preferido (GitHub, GitLab, BitBucket, etc) e enviar o link da aplicação.


## Como se destacar

- Escrever teste unitários
- Fazer deploy em algum ambiente cloud(AWS, GCE, Heroku)
- Seguir os [12 fatores de aplicação](https://12factor.net/)


Lembre-se que seu código será analisado por desenvolvedores.
Não economize nos comentários nos códigos e de commit.

